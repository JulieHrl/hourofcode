Notre tout premier projet de groupe, en tout début de formation.

L'objectif: faire une initiation au code au collège dans le cadre de l'hour of code, pour des enfants d'environ 13 ans.

Les enfants devaient identifier le code leur permettant de modifier la trajectoire de la fusée afin de sauver la terre,

et enfin trouver le code du bouton rouge et le décommenter pour permettre son exécution.