$( document ).ready(function() { 
    let temp = '3000'; /*durée de vol de la fusée*/
    let Xarr = '500'; /*coordonnées d'arrivée: influe sur le déplacement horizontal*/
    let Yarr = '350'; /*coordonnées d'arrivée: influe sur le déplacement vertical*/
    let btns = $("#start");
    let btnr = $("#reset");
    let btnb = $("#btboom");
    let fus = $("#rocket");
    let posdepart = fus.position();
  /*Tout ce qui commence par "let" est une variable: ce sont des éléments auxquels on peut attribuer des propriétés, des valeurs par exemple, et qui permettent de faire le lien avec le html et le css; on va pouvoir leur donner des instructions pour faire des animations*/
    console.log(posdepart);
              
              
    btns.click(function(){ /*quand le bouton "lancer la fusée" est cliqué, cette fonction s'execute: c'est un ensemble d'instructions données à la fusée*/
      if (420< Xarr && Xarr < 860) {
        fus.css({'width' : '0px', 'height' : '0px' , 'left' : Xarr+'px', 'top' : Yarr+'px', "transition-duration": temp+'ms'});  
        $("#boom").css({'left' : Xarr+'px', 'top' : Yarr+'px', 'transform' : 'translate(-50%,-50%)'});
        $("#boom").delay(temp).fadeIn(0);
        $("#text").delay(temp).html("VOUS AVEZ PERDU !"); 
      }
      else{
        fus.css({'width' : '0px', 'height' : '0px' , 'left' : Xarr+'px', 'top' : Yarr+'px', "transition-duration": temp+'ms'});
        $("#text").html("VOUS AVEZ GAGNÉ !");  
      } /* ici on a une condition: "si la fusée va ici, on execute certaines actions" "sinon on execute d'autres actions". C'est le "if" et le "else"*/
  
    })
    btnr.click(function () { /*quand le bouton "reset" est cliqué, cette fonction s'execute: elle fait revenir la fusée à sa position de départ*/
      fus.css({'width' : '300px', 'height' : '300px', 'left' : '0px', 'top' : '500px'});
      $("#boom").fadeOut(0);
      $("#groboom").fadeOut(0);
      $("#text").html("");
    })
    
    
    /* btnb.click(function(){
                  $("#groboom").css({'left' :'350px', 'top' : '50px', width : "600px"});
                  $("#groboom").fadeIn(0); 
                  $("#text").html("VOUS AVEZ PERDU !");
              })*/
  });
  /*En JavaScript, on commente de la même manière qu'en CSS. Si on a du code en commentaire, il suffit d'enlever les /* pour que le code s'execute. Attention, il peut y avoir du code caché dans les différentes pages html, css, JS */